import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    email: string;
    password: string;
    cnfpassword: string;

  constructor() { }

  ngOnInit() {
  }
  signup() {

    console.log(this.email);
    console.log(this.password);
    console.log(this.cnfpassword);

    if (this.password !== this.cnfpassword) {
      alert('Password not same');
    } else {
      alert('Registration Successful');
    }
  }
}
